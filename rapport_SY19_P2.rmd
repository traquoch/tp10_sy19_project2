---
title: <div style="text-align:center;"> <strong> Rapport SY19 TP10 A21 </strong></div>
author:   "Lelong Philomène, Sarbout Ilias, Tran Quoc Hung"
output:
  pdf_document: default
  html_document:
    df_print: paged
editor_options: 
  chunk_output_type: inline
---
```{r setup, include=FALSE} 
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
```
\tableofcontents

# Introduction

Ce rapport présente les différentes méthodes appliquées pour la résolution de deux problèmes de classification dans une première partie puis pour un problème de régression dans une seconde. La méthode donnant le meilleur modèle pour chacun des problèmes a été retenue et déposer sur http://maggle.gi.utc/. Pour nos deux problèmes, un partitionnement des données en un ensemble de données d’apprentissage et un ensemble de données de test est effectué ou une création de fonctions de validationcroisée car cela permet la comparaison  objective des modèles construits, grâce aux données d’apprentissage, avec une application sur les données de test. Nous avons fait le choix de créer nos modèles de classification et de régression avec des données non-standardisées. Le format du fichier .Rdata attendu rendait la tâche de standardisation de l'ensemble des données (données du fichier et test_set) compliquée. De plus, standardiser les données du fichier d'apprentissage et du fichier de test indépendamment montrait à l'inverse une tendance à accentuer le taux de mauvaise classification ou la MSE.

# Jeu de données 1 : Problème de classification

## Prétraitement et Analyse Exploratoire :
### infos jeu de données :

  Le problème traité dans cette première partie est un problème de classification. La variable à expliquer, y, est qualitative nominale à 5 modalités  {aa,ao,dcl,iy,sh}.  Pour prédire cette variable, on dispose de 256 variables explicatives quantitatives dont les valeurs semblent être centrées et réduites. Nous disposons de 2250 relevés complets. 
  Nous noterons n, le nombre de relevés et p, le nombre de variables explicatives. Ici, nous avons p et n grands.

L'impression de la trame de données nous donne plusieurs informations. Voici le résumé de la base de données de classification, pour les 6 premières variables prédictives. 
  
```{r}
library(e1071)
library(randomForest)
library(nnet)
library(ggplot2)
library(methods)
library(MASS)
library(naivebayes)
library(caret)
library(nnet)
library(mclust)
library(mda)
#traitement des données
#data =  read.table('C:/Users/philo/OneDrive/Documents/A21/SY19/Projet_2/rapport/DonnéesTP10/parole_train.txt')
setwd("/home/tranquochung/Desktop/SY19/TD10")  # path de TRAN
data = read.table("parole_train.txt")
data = data[complete.cases(data),]
n<- nrow(data)
p<- ncol(data)
row.names(data) <- 1:nrow(data)
lev = levels(as.factor(data$y))
set.seed(42)
rows <- sample(nrow(data)) 
data <- data[rows, ]
rownames(data) <- 1:nrow(data)
head(data[,1:6])
```


## I. Analyse Préliminaire et Pré-traitement des données

Nous avons voulu réduire le nombre de variables explicatives afin de rendre les résultats plus interprétables et éviter le surapprentissage, mais au vu des corrélations toutes comprises entre -0,1 et 0,1, nous avons décidé de garder toutes les variables explicatives.

```{r}
pca      <- princomp(scale(data[,1:256]))
pca_data <- cbind(pca$scores,data$y)
pca_data <- data.frame(pca_data)
colnames(pca_data) <- colnames(data)
pca_data$y <- factor(pca_data$y)

#calculate total variance explained by each principal component
var_explained = pca$sdev^2 / sum(pca$sdev^2)

#create scree plot
qplot(c(1:256), var_explained[]) + 
  geom_line() + 
  xlab("Principal Component") + 
  ylab("Variance Explained") +
  ggtitle("Scree Plot") +
  ylim(0, 1)
```

PC1 explique 55% de la variance totale, ce qui signifie que près des un demi tiers des informations de l'ensemble de données (256 variables) peuvent être encapsulées par cette seule composante principale. PC2 explique 15 % de la variance. Ainsi, en connaissant la position d'un échantillon par rapport uniquement à PC1 et PC2, vous pouvez obtenir une vue très précise de sa position par rapport aux autres échantillons, car seuls PC1 et PC2 peuvent expliquer 70% de la variance.
```{r}
color_pallete_function <- colorRampPalette(
  colors = c("red", "green", "black", "blue","yellow"),
  space = "Lab" # Option used when colors do not represent a quantitative scale
)
num_colors <- nlevels(pca_data$y)
colors <- color_pallete_function(num_colors)
y_colors <- colors[pca_data$y]
plot(pca_data$X1,pca_data$X2,col=y_colors,xlim = c(-40, 30))
legend(
  x = "topleft",
  legend = paste("Color", levels(pca_data$y)), # for readability of legend
  col = colors,
  pch = 19, # same as pch=20, just smaller
  cex = .7 # scale the legend to look attractively sized
)
```
  
La projection sur les axes factoriels révèle trois clusters dont deux sont facilement assimilables à des classes de phonemes distinctes.

## Méthodes utilisées :
Méthodologie de comparaison de modèles:

Nous allons maintenant créer une fonction de validation croisée que nous utiliserons plus tard, pour comparer le résultat des différents modèles de classification comme nous l'avons fait pour la régression. Si nous n'utilisions qu'un seul ensemble de tests pour comparer la précision de notre modèle sur, les résultats seraient biaisés. Par conséquent, pour une méthode de classification donnée, notre fonction effectuera les opérations suivantes :

- Mettre le seed au même numéro (42) pour la méthode de classification donnée, pour des raisons de reproductibilité

- Diviser l'ensemble de données en K sous-ensembles (plus ou moins 1 observation)

- Effectuer une k-validation croisée comme vu en classe : former les modèles K-1 obtenus avec la méthode donnée sur K-1 différents ensembles de données, obtenus en « laissant de côté » l'un des K sous-ensembles de données à chaque itération de notre boucle de fonction. Puis également à chaque itération, notre fonction de validation croisée testera chaque modèle trouvé sur le k sous-ensemble de données laissé de côté d'évaluer leur exactitude en calculant l'erreur CV. L'erreur CV est ici l'erreur de probabilité représentant l'erreur de classement (calculée avec la matrice de confusion). Bien sûr, s'il y a des hyperparamètres à estimé alors nous allons effectuer un CV interne en utilisant tous les plis à l'exception du pli k

Enfin, la moyenne des k cv-erreurs est calculée et renvoyée. De cette façon, l'erreur de précision du modèle calculé est moins
biaisé, et le fait que la graine est toujours définie sur 42 pour évaluer la précision de tous nos modèles avec 10-CV
nous permet de comparer les k cv-erreurs moyennes de chacun de nos modèles

```{r}
cross_validation <- function(data, model, folds) {
  n <- nrow(data)
  p <- ncol(data)
  ntst <- n/folds
  
  set.seed(17)
  fold_ids <- rep(seq(folds), ceiling(n / folds))
  fold_ids <- fold_ids[1:n]
  fold_ids <- sample(fold_ids, length(fold_ids))
  
  CV_model_accuracy  <- vector(length = folds, mode = "numeric")
  
  ## Loop ##
  for (k in (1:folds)) {
    if(model == "LDA") {
      CV_model_accuracy[k] <- LDA_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }
    else if(model == "QDA") {
      CV_model_accuracy[k] <- QDA_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }
    else if(model == "NAIVE_BAYES") {
      CV_model_accuracy[k] <- NBAYES_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }
    else if(model == "FDA") {
      CV_model_accuracy[k] <- FDA_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }
    else if(model == "RDA") {
      CV_model_accuracy[k] <- RDA_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }
   else if(model == "KNN") {
      CV_model_accuracy[k] <- KNN_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
  }
  else if(model == "SVM_RADIAL"){
      CV_model_accuracy[k] <- SVM_RADIAL_accuracy(
                    traindata=data[which(fold_ids != k),], 
                    testdata=data[which(fold_ids == k),]
                    )
  }
   else if(model == "SVM_LINEAR"){
      CV_model_accuracy[k] <- SVM_LINEAR_accuracy(
                    traindata=data[which(fold_ids != k),], 
                    testdata=data[which(fold_ids == k),]
                    )
  }
  else if(model == "RF") {
      CV_model_accuracy[k] <- RF_accuracy(y~., 
                                           traindata=data[which(fold_ids != k),], 
                                           testdata=data[which(fold_ids == k),]
                                           )
    }

 }
   noquote(sprintf("Mean accuracy %s : %.3f",model, mean(CV_model_accuracy)))
}
```

Pour chaque méthode : LDA, QDA, Naïve Bayes, Random Forest, nous avons fait un test en définisant chaque fonction qui renvoie la précision de chaque méthode. Pour chaque test, les erreurs moyennes de nos modèles ont été sauvegardées et renvoyées à la fin sous la forme d'une trame de données. 

Pour RDA methode, on recherche des meilleures paramètres (alpha et gamma ) par gridsearch (hold out) puis estimation des performances en validation croisée
=> on a trouvé alpha = 0.53 et lambda = 0.92.


```{r}
library(klaR)

###### RDA with gamma = 0.53, lampda = 0.92 ######
RDA_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  model.rda <- rda(formula, data=traindata, gamma = 0.53, lambda = 0.92)
  pred.rda  <- predict(model.rda, newdata=testdata)
  accuracy.rda <- mean(pred.rda$class == testdata[,p])
  return(accuracy.rda)
}



###### LDA ######
LDA_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  model.lda <- lda(formula, data=traindata)
  pred.lda <- predict(model.lda, newdata=testdata[,-p])
  accuracy.lda <- mean(pred.lda$class == testdata[,p])
  return(accuracy.lda)
}

###### QDA ######
QDA_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  model.qda <- qda(formula, data=traindata)
  pred.qda <- predict(model.qda, newdata=testdata[,-p])
  accuracy.qda <- mean(pred.qda$class == testdata[,p])
  return(accuracy.qda)
}

###### NAIVE BAYES ######

NBAYES_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  traindata$y <- as.factor(traindata$y)
  testdata$y <- as.factor(testdata$y)
  model.nbayes <- naive_bayes(formula, data=traindata)
  pred.nbayes <- predict(model.nbayes, newdata=testdata[,-p])
  accuracy.nbayes <- mean(pred.nbayes == testdata[,p])
  return(accuracy.nbayes)
}

######  FDA  ######

FDA_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  model.fda <- princomp(formula, data=traindata)
  print( model.fda )

  pred.fda <- predict(model.fda, newdata=testdata[,-p])
  accuracy.fda <- mean(pred.fda == testdata[,p])
  return(accuracy.fda)
}

###### KNN ######

KNN_accuracy <- function(formula, traindata, testdata) {
  set.seed(123)
  p <- ncol(traindata)
  model.knn <- train(
    y ~., data = traindata, method = "knn",
    trControl = trainControl("cv", number = 10),
    preProcess = c("center","scale"),
    tuneLength = 20
  )
  pred.knn <- knn(train = traindata, test = testdata, cl = traindata$y, k=model.knn$bestTune)
  accuracy.knn <- mean(pred.knn == testdata$y)
  return(accuracy.knn)
}


######  SVM_RADIAL  ######

SVM_RADIAL_accuracy <- function(traindata, testdata) 
{
  x = subset(traindata, select = -y)
  p <- ncol(traindata)
  model.svm.radial     <- svm(x, data.frame(factor(traindata$y)), kernel="radial" , scale=F, type = "C-classification")
  pred.svm.radial      <- predict(model.svm.radial , newdata=testdata[,-p])
  accuracy.svm.radial  <- mean(pred.svm.radial == testdata[,p])
  return(  accuracy.svm.radial)
}

######  SVM_LINEAR  ######

SVM_LINEAR_accuracy <- function(traindata, testdata) 
{
  x = subset(traindata, select = -y)
  p <- ncol(traindata)
  model.svm.linear     <- svm(x, data.frame(factor(traindata$y)), kernel="linear" , scale=F, type = "C-classification")
  pred.svm.linear      <- predict(model.svm.linear , newdata=testdata[,-p])
  accuracy.svm.linear  <- mean(pred.svm.linear == testdata[,p])
  return(  accuracy.svm.linear)
}

######  Random Forest  ######

RF_accuracy <- function(formula, traindata, testdata) 
{
  p <- ncol(traindata)
  model.RF             <- randomForest(as.factor(y)~.,data=traindata, ntree = 500, mtry = 5)
  pred.RF              <- predict( model.RF , newdata=testdata)
  perf                 <-table(testdata$y  , pred.RF)
  accuracy.RF = (sum(diag(perf))/sum(perf))
  return(accuracy.RF)
}
```



Voici les résultats que nous avons obtenus :

```{r}
cross_validation(data, "LDA"              , 10)
cross_validation(data, "QDA"              , 10)
cross_validation(data, "NAIVE_BAYES"      , 10)
cross_validation(data, "SVM_RADIAL"       , 10)
cross_validation(data, "SVM_LINEAR"       , 10)
cross_validation(data, "RF"               , 10)
cross_validation(data, "RDA"               , 10)
```

Remaques : 

- Le méthode QDA, il semble meilleur que LDA, avec une précision d'environ 67% ou un taux d'erreur de 33%, ce qui signifie que la supposition de l'égalité des matrices de covariance pourrait être raisonnable.


## Meilleurs résultats :

RDA ou SVM_rad

## Explication :

L'analyse discriminante englobe des méthodes qui peuvent être utilisées à la fois pour la classification et la réduction de la dimensionnalité. L'analyse discriminante linéaire (LDA) est particulièrement populaire car elle est à la fois un classificateur et une technique de réduction de dimensionnalité. L'analyse discriminante quadratique (QDA) est une variante de LDA qui permet une séparation non linéaire des données. Enfin, l'analyse discriminante régularisée (RDA) est un compromis entre LDA et QDA. Pour le dataset1, il a 256 variables =>  dimensions élevé, RDA est saistifait méthod que l'on a essayé.  

Les avantages des machines à vecteurs supports sont : Efficace dans les espaces de grande dimension. Toujours efficace dans les cas où le nombre de dimensions est supérieur au nombre d'échantillons.il semble que le SVM du noyau linéaire et RBF fonctionnerait aussi bien sur cet ensemble de données. Alors, pourquoi préférer l'hypothèse linéaire plus simple ? Pensez au rasoir d'Occam dans ce cas particulier. Le SVM linéaire est un modèle paramétrique, un SVM à noyau RBF ne l'est pas, et la complexité de ce dernier augmente avec la taille de l'ensemble d'apprentissage. La présion de radial SVM est mieux c'est à dire noyaux non linéaires tels que le noyau de la fonction de base radiale pour les problèmes non linéaires. 

Pour obtenir l'erreur de test la moins biaisée pour notre meilleur modèle de prédiction, nous allons procéder comme suit : nous allons ré-estimer les paramètres du modèle en utilisant l'ensemble de données d'apprentissage construit ci-dessus. Et pour obtenir une estimation impartiale de la meilleure erreur de modèle, nous utiliserons notre ensemble de validation créé ci-dessus comme ensemble de test indépendant. On retrouve finalement ces résultats. À la fin, c'est RDA qui a donné les meilleurs résultats, et donc, nous avons testé son efficacité en l'entraînant sur un sous-ensemble d'apprentissage de nos données, avant de le tester par rapport aux données de test restantes. :


## Pour améliorer :
Faire du grid-search pour réseau de neuronne pour retrouver modèle permettant d'obtenir 0,944

# Jeu de données 2 : Problème de classification

## Objectif :
prédire la lettre de l'alphabet à partir des 16 attributs primitifs

## Prétraitement et Analyse Exploratoire :
### infos jeu de données :

On déplace la column de target du data frame à la fin pour s'adapter la fonction cross_validation qui a défini pour les classification problèmes.

L'impression de la trame de données nous donne plusieurs informations. Voici le résumé de la base de données de classification, pour les 6 premières variables prédictives. 


```{r}
data2 = read.table("letters_train.txt")
data2 = data2[complete.cases(data2),]
n2<- nrow(data2)
p2<- ncol(data2)
row.names(data2) <- 1:nrow(data2)
lev = levels(as.factor(data2$y))
set.seed(42)
rows <- sample(nrow(data2)) 
data <- data2[rows, ]
rownames(data2) <- 1:nrow(data2)


d = subset(data2, select= -Y)
y = subset(data2, select= Y)
d$y = y$Y

data2 = d

head(data2)
```

- 16 variables explicatives
- 1 variable à expliquée de type qualitative nominale (26 valeus possibles)
- 10 000 relevés

on notera n2, le nombre de relevés et p2, le nombre de variables explicative.
ici, on a p2 petit et n2 grand.

### ACP

## Methodes utilisées :

Comme l'exercice 1, on applique la fonction cross_validation pour l'ensemble de données pour chaque method pour trouver le meilleur resultat.

Pour le méthode RDA, on trouve les paramètres de réglage sont égaux à 0. Ce qui équivalente à QDA. 

Pour radial SVM methods, on utilize 10-fold validation croisée pour trouver des paramètres de réglage (cost ou 'C'-constante du terme de régularisation dans la formulation de Lagrange.). ici cost est égal à 20.

On redéfini la fonction RDA_accuracy et SVM_RADIAL_accuracy avec ses nouveaux parametres. 


```{r}
RDA_accuracy <- function(formula, traindata, testdata) {
  p <- ncol(traindata)
  model.rda <- rda(formula, data=traindata, gamma = 0, lambda = 0)
  pred.rda  <- predict(model.rda, newdata=testdata)
  accuracy.rda <- mean(pred.rda$class == testdata[,p])
  return(accuracy.rda)
}


######  SVM_RADIAL  ######

SVM_RADIAL_accuracy <- function(traindata, testdata) 
{
  x = subset(traindata, select = -y)
  p <- ncol(traindata)
  model.svm.radial     <- svm(x, data.frame(factor(traindata$y)), kernel="radial" , scale=F, type = "C-classification", cost = 40)
  pred.svm.radial      <- predict(model.svm.radial , newdata=testdata[,-p])
  accuracy.svm.radial  <- mean(pred.svm.radial == testdata[,p])
  return(  accuracy.svm.radial)
}

```


```{r}
cross_validation(data2, "LDA"              , 10)
cross_validation(data2, "QDA"              , 10)
cross_validation(data2, "NAIVE_BAYES"      , 10)
cross_validation(data2, "SVM_RADIAL"       , 10) # with C = 40
cross_validation(data2, "SVM_LINEAR"       , 10)
cross_validation(data2, "RF"               , 10)
cross_validation(data2, "RDA"              , 10)
```

- Réseau de neuronnes ?

```{r}
install.packages("keras")
library(keras)
```


## Meilleur résultat :


radial SVM      = 0.957


Forêt aléatoire = 0.948


## explication :

justifier les résultats obtenus

# Conclusion problèmes de classification :

Expliquer pourquoi meilleur résultat = SVM rad ou RDA dataset 1
Expliquer pourquoi meilleur résultat = radial SVM dataset 2

 SVM : 
 - Très efficaces quand on ne dispose que de peu de données d’entraînements 
 - Baisse en performance qd données sont trop nombreuses
 - La séparation des classes est souvent très non linéaire. La possibilité d'appliquer de nouveaux noyaux permet une flexibilité substantielle pour les limites de décision, conduisant à de meilleures performances de classification.
 
RF :
-  Random forest = tree bagging + feature sampling : aide à lutter contre la forte dépendance aux données de l'échantillon de départ.
- performant avec un nombre de variables explicatives important.
- les forêts aléatoires effet boite noir (résultats peu lisibles)

RDA : très adaptée au dataset 1
- DA particulièrement utiles pour pb multiclasses 
- LDA très interprétable car autorise reduction des dimensions - QDA permet de représenter une relation non-linéaire. 
- RDA une technique particulièrement utile  avec un nombre de variables explicatives important

## Différences dans meilleur modèle entre dataset 1 et 2 du à : 
=> nb valeurs var à expliquer (5 pour 1 et 26 pour 2)
=> nb var explicative (256 pour 1 et 16 pour 2)
=> nb relevé mais les 2 très grand (2250 pour 1 et 10000 pour 2

#Jeu de données 3 : Problème de régression

## objectifs :
- prédire cnt en fonction des conditions environnemental et saisonnal 
- utiliser **au minimum 4 ML étudier** en classe
- déterminer quelles sont les variables qui influent le plus sur le nombre de locations
- analyser le sens de cette influence.

## Prétraitement et Analyse Exploratoire :
ACP

## Analyse de l'influence des variables 

voir graphe ACP + corrélations

on remarque :

- que atemp et temp très corrélé 
- que season et month corrélés
- que cnt très corrélé à atemp et temp

On s'intéresse aux fleches les plus grandes car plus elles sont grande mieux elles sont représentée par les axes. On voit ici que cnt,atemp,temp, season, month, weather, hum soemble être bien représentées.
comme on remarque que cnt suit la meme direction que temp et atemp, on peut en déduire qu'ils sont positivement corrélé.
autre coté de l'axe que weather 
weather grd => mauvais tps

Conclusion :



- les variables qui influent le plus sur le nombre de locations sont temp et atemp, le sens de cette influence est positive car elles sont positivement corrélées.
- Ensuite, la variable saison et mois influent également de manière positive.
- Pour finir, la variable weather influent dans les sens négatif.

## Methodes utilisées :
- Forêt aléatoire
- GAM
- Réseau de neuronnes
- methode ascendante (avec BIC, R^2 , CV, Holdout)
- méthode de régularisation (Ridge, Lasso, Elastic net)
- Support Vector Regression : résultat dépend énormément de l'ensemble d'apprentissage

sur l'ensemble des données, meilleur resultat = env. 400 000 pour C=1000 et eps=0.001 

sinon sur certains échantillons 300 000 pour C=10e4 et eps=0.001

## Meilleur résultat
forêts aléatoires